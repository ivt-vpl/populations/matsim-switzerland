package ch.ethz.matsim.switzerland.preparation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Person;
import org.matsim.households.Household;

public class ScenarioPreparator {
	static public void adjustPopulation(Scenario scenario) {
		for (Household household : scenario.getHouseholds().getHouseholds().values()) {
			for (Id<Person> memberId : household.getMemberIds()) {
				Person person = scenario.getPopulation().getPersons().get(memberId);

				if (person != null) {
					person.getAttributes().putAttribute("bikeAvailability",
							household.getAttributes().getAttribute("bikeAvailability"));
					person.getAttributes().putAttribute("spRegion", household.getAttributes().getAttribute("spRegion"));
				}
			}
		}
	}

	static public void adjustNetwork(Scenario scenario) {
		for (Link link : scenario.getNetwork().getLinks().values()) {
			Set<String> allowedModes = new HashSet<>(link.getAllowedModes());

			if (allowedModes.contains("car")) {
				allowedModes.addAll(Arrays.asList("car_passenger"));
			}

			link.setAllowedModes(allowedModes);
		}
	}
}
