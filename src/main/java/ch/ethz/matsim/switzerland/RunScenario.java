package ch.ethz.matsim.switzerland;

import org.matsim.api.core.v01.Scenario;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.scenario.ScenarioUtils;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.baseline_scenario.traffic.BaselineTrafficModule;
import ch.ethz.matsim.baseline_scenario.transit.BaselineTransitModule;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.ethz.matsim.discrete_mode_choice.modules.DiscreteModeChoiceModule;
import ch.ethz.matsim.discrete_mode_choice.modules.config.DiscreteModeChoiceConfigGroup;
import ch.ethz.matsim.switzerland.config.SwissQSimModule;
import ch.ethz.matsim.switzerland.config.SwitzerlandConfigGroup;
import ch.ethz.matsim.switzerland.mode_choice.SwissModeChoiceModule;
import ch.ethz.matsim.switzerland.preparation.ScenarioPreparator;
import ch.sbb.matsim.routing.pt.raptor.SwissRailRaptorModule;

public class RunScenario {
	static public void main(String[] args) throws ConfigurationException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("config-path") //
				.allowPrefixes("mode-choice-parameter", "cost-parameter") //
				.build();

		// Load config
		Config config = ConfigUtils.loadConfig(cmd.getOptionStrict("config-path"), //
				new SwitzerlandConfigGroup(), //
				new DiscreteModeChoiceConfigGroup());
		cmd.applyConfiguration(config);

		// Load scenario
		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		// Adjust scenario
		ScenarioPreparator.adjustPopulation(scenario);
		ScenarioPreparator.adjustNetwork(scenario);

		// Set up controller
		Controler controller = new Controler(scenario);
		controller.addOverridingModule(new SwissRailRaptorModule());
		controller.addOverridingModule(new BaselineTransitModule());
		controller.addOverridingModule(new BaselineTrafficModule(3.0));

		controller.addOverridingModule(new DiscreteModeChoiceModule());
		controller.addOverridingModule(new SwissModeChoiceModule(cmd));

		controller.addOverridingModule(new SwissQSimModule());

		// Run
		controller.run();
	}
}
