package ch.ethz.matsim.switzerland;

import java.util.Arrays;

import org.matsim.api.core.v01.TransportMode;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.config.ConfigWriter;
import org.matsim.core.config.groups.ControlerConfigGroup.RoutingAlgorithmType;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ModeParams;
import org.matsim.core.config.groups.PlansCalcRouteConfigGroup.ModeRoutingParams;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.discrete_mode_choice.modules.ConstraintModule;
import ch.ethz.matsim.discrete_mode_choice.modules.DiscreteModeChoiceConfigurator;
import ch.ethz.matsim.discrete_mode_choice.modules.EstimatorModule;
import ch.ethz.matsim.discrete_mode_choice.modules.ModelModule.ModelType;
import ch.ethz.matsim.discrete_mode_choice.modules.SelectorModule;
import ch.ethz.matsim.discrete_mode_choice.modules.config.DiscreteModeChoiceConfigGroup;
import ch.ethz.matsim.discrete_mode_choice.modules.config.VehicleTourConstraintConfigGroup.HomeType;
import ch.ethz.matsim.switzerland.config.SwitzerlandConfigGroup;
import ch.ethz.matsim.switzerland.mode_choice.SwissModeChoiceModule;

public class UpdateConfig {
	static public void main(String[] args) throws ConfigurationException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("input-path", "output-path") //
				.allowOptions("use-discrete-mode-choice") //
				.build();

		Config config = ConfigUtils.loadConfig(cmd.getOptionStrict("input-path"), //
				new SwitzerlandConfigGroup(), //
				new DiscreteModeChoiceConfigGroup());

		boolean useDiscreteModeChoice = cmd.getOption("use-discrete-mode-choice").map(Boolean::parseBoolean)
				.orElse(true);

		// Add Discrete Mode Choice
		if (useDiscreteModeChoice) {
			DiscreteModeChoiceConfigurator.configureAsModeChoiceInTheLoop(config);
			DiscreteModeChoiceConfigGroup dmcConfig = (DiscreteModeChoiceConfigGroup) config.getModules()
					.get(DiscreteModeChoiceConfigGroup.GROUP_NAME);

			dmcConfig.setModelType(ModelType.Tour);
			dmcConfig.setPerformReroute(false);

			dmcConfig.setSelector(SelectorModule.MULTINOMIAL_LOGIT);

			dmcConfig.setTripEstimator(SwissModeChoiceModule.UTILITY_ESTIMATOR_NAME);
			dmcConfig.setTourEstimator(EstimatorModule.CUMULATIVE);
			dmcConfig.setCachedModes(Arrays.asList("car", "bike", "pt", "walk", "car_passenger", "truck"));

			dmcConfig.setTourFinder(SwissModeChoiceModule.TOUR_FINDER_NAME);
			dmcConfig.setModeAvailability(SwissModeChoiceModule.MODE_AVAILABILITY_NAME);

			dmcConfig.setTourConstraints(
					Arrays.asList(ConstraintModule.VEHICLE_CONTINUITY, ConstraintModule.FROM_TRIP_BASED));
			dmcConfig.setTripConstraints(Arrays.asList(ConstraintModule.TRANSIT_WALK,
					SwissModeChoiceModule.PASSENGER_CONSTRAINT_NAME, SwissModeChoiceModule.OUTSIDE_CONSTRAINT_NAME));

			dmcConfig.getVehicleTourConstraintConfig().setHomeType(HomeType.USE_ACTIVITY_TYPE);
			dmcConfig.getVehicleTourConstraintConfig().setRestrictedModes(Arrays.asList("car", "bike"));

			dmcConfig.setTourFilters(Arrays.asList(SwissModeChoiceModule.OUTSIDE_FILTER_NAME,
					SwissModeChoiceModule.TOUR_LENGTH_FILTER_NAME));
		}

		// These parameters are only used by SwissRailRaptor. We configure the
		// parameters here in a way that SRR searches for the route with the shortest
		// travel time.
		PlanCalcScoreConfigGroup scoringConfig = config.planCalcScore();

		ModeParams ptParams = scoringConfig.getModes().get(TransportMode.pt);
		ptParams.setConstant(0.0);
		ptParams.setMarginalUtilityOfDistance(0.0);
		ptParams.setMarginalUtilityOfTraveling(-1.0);
		ptParams.setMonetaryDistanceRate(0.0);

		scoringConfig.setMarginalUtilityOfMoney(0.0);
		scoringConfig.setMarginalUtlOfWaitingPt_utils_hr(0.0);

		// Add unknown modes
		scoringConfig.getOrCreateModeParams("outside");
		scoringConfig.getOrCreateModeParams("car_passenger");
		scoringConfig.getOrCreateModeParams("freight");

		// Some additional settings
		config.controler().setRoutingAlgorithmType(RoutingAlgorithmType.FastAStarLandmarks);

		ModeRoutingParams bikeParams = config.plansCalcRoute().getModeRoutingParams().get(TransportMode.bike);
		bikeParams.setBeelineDistanceFactor(1.4);
		bikeParams.setTeleportedModeSpeed(3.1); // 11.6 km/h

		ModeRoutingParams walkParams = config.plansCalcRoute().getModeRoutingParams().get(TransportMode.walk);
		walkParams.setBeelineDistanceFactor(1.3);
		walkParams.setTeleportedModeSpeed(1.2); // 4.32 km/h

		// Iterations
		config.controler().setLastIteration(40);

		// Write config
		new ConfigWriter(config).write(cmd.getOptionStrict("output-path"));
	}
}
