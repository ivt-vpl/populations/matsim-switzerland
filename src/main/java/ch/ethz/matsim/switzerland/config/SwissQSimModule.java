package ch.ethz.matsim.switzerland.config;

import java.util.ArrayList;
import java.util.Collection;

import org.matsim.core.config.Config;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.mobsim.qsim.AbstractQSimPlugin;
import org.matsim.core.mobsim.qsim.ActivityEnginePlugin;
import org.matsim.core.mobsim.qsim.PopulationPlugin;
import org.matsim.core.mobsim.qsim.TeleportationPlugin;
import org.matsim.core.mobsim.qsim.changeeventsengine.NetworkChangeEventsPlugin;
import org.matsim.core.mobsim.qsim.messagequeueengine.MessageQueuePlugin;
import org.matsim.core.mobsim.qsim.qnetsimengine.QNetsimEnginePlugin;

import com.google.inject.Provides;
import com.google.inject.Singleton;

import ch.ethz.matsim.baseline_scenario.transit.simulation.BaselineTransitPlugin;

public class SwissQSimModule extends AbstractModule {
	@Override
	public void install() {
	}

	@Provides
	@Singleton
	public Collection<AbstractQSimPlugin> provideAbstractQSimPlugins(Config config) {
		final Collection<AbstractQSimPlugin> plugins = new ArrayList<>();

		plugins.add(new MessageQueuePlugin(config));
		plugins.add(new ActivityEnginePlugin(config));
		plugins.add(new QNetsimEnginePlugin(config));

		if (config.network().isTimeVariantNetwork()) {
			plugins.add(new NetworkChangeEventsPlugin(config));
		}

		if (config.transit().isUseTransit()) {
			// This is the only change to the standard QSim plugins
			plugins.add(new BaselineTransitPlugin(config));
		}

		plugins.add(new TeleportationPlugin(config));
		plugins.add(new PopulationPlugin(config));

		return plugins;
	}
}
