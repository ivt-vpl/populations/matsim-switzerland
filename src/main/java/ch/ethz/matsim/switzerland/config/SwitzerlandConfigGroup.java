package ch.ethz.matsim.switzerland.config;

import org.matsim.core.config.ReflectiveConfigGroup;

public class SwitzerlandConfigGroup extends ReflectiveConfigGroup {
	public static final String GROUP_NAME = "switzerland";

	public SwitzerlandConfigGroup() {
		super(GROUP_NAME);
	}

	// For future configuration options
}
