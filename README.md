# MATSim Switzerland

This repository contains a reference implementation of the IVT MATSim simulation for Switzerland. 

## Getting the scenario

The scenario including the population file, network file, etc. is with a consistent pipeline. It transforms raw data sets into a runnable MATSim scenario. The pipeline itself can be found in the following repository: [Synthetic population of Switzerland](https://gitlab.ethz.ch/ivt-vpl/populations/ch-zh-synpop). There, it is described in detail how to create a new scenario.

The scenario pipeline takes care of the low-level preparations. In case minor changes should be applied to the scenario (like adjusting the network, the transit schedule, ...) this can be done based on a full output of the scenario pipeline. The current version of the Switzerland sceanrio can be found on Euler in

> /cluster/work/ivt_vpl/matsim_switzerland

In case you do not have access, please contact one of the people working with MATSim at IVT. The folder contains a number of sample sizes for the latest version of the scenario pipeline: `1pm` (for 0.1%), `1pct` (1%), `10pct`, and `25pct`. For testing purpose the first is most useful, since it can easily be run on a normal computer.

## Running the scenario

Once the files are in place (we assume there is a, e.g. `switzerland_1pm` directory containing `switzerland_population.xml.gz` and all other relevant files), the simulation can be started. For that there are a couple of options. In any case, it makes sense to first clone this repository into a folder, e.g. `matsim-switzerland`. This folder can then either be important to Eclipse or IntelliJ as a "Maven project", or it can be build from the command line using `mvn package`.

Currently, the pipeline is not producing a 100% configured scenario. For that, there is the runnable class `UpdateConfig`, which sets some default values for the baseline scenario. It expects two command line options: `--input-path` stating the location of the scenario config file, `--output-path` specifying where the updated config file should be put. The class can be run directly in Eclipse or via command line:

```sh
java -Xmx10G -cp matsim-switzerland/target/matsim-switzerland-1.0.jar ch.ethz.matsim.switzerland.UpdateConfig --input-path switzerland_1pm/switzerland_config.xml --output-path switzerland_1pm/updated_config.xml
```

The relevant class to run is `RunScenario`. It requires at least one command line argument `--config-path`, which should point to the config file of the scenario that should be run. Command line options can be set up in Eclipse in the "run configurations". From the command line an example run of the scenario can be started as follows:

```sh
java -Xmx10G -cp matsim-switzerland/target/matsim-switzerland-1.0.jar ch.ethz.matsim.switzerland.RunScenario --config-path switzerland_1pm/updated_config.xml
```

Please note that the scenario pipeline does not yet properly set up the capacity scaling dependent on the sample size of the scenario. For that, one should set appropriate values either in the config file itself, or via command line arguments:

```sh
java [...] ch.ethz.matsim.switzerland.RunScenario --config-path [...] --config:qsim.storageCapacityFactor 0.1 --config:qsim.flowCapacityFactor 0.1
```

## Further information

Unfortunately, there is no comprehensive documentation of the scenario at the moment which would detail all the components, models, etc. that are involved. This is on our TODO list and should come soon!
